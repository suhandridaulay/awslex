<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Conversations;
use Aws\LexModelsV2\LexModelsV2Client;
use Illuminate\Http\Request;
use Aws\Exception\AwsException;
use Aws\LexRuntimeV2\LexRuntimeV2Client;
use Illuminate\Support\Facades\Session;


class ChatController extends Controller
{

    public function index()
    {

        $sessionid = Session::token();
        if (!User::where('sessionid', $sessionid)->exists()) {
            User::create([
                'sessionid' => $sessionid
            ]);
        }
        Session::put('sessionid',  $sessionid);
        $client = new LexRuntimeV2Client([
            'profile' => 'default',
            'version' => 'latest',
            'region'  => 'us-west-2'
        ]);
        
        $result = $client->putSession([
            'botAliasId' => 'TSTALIASID', // REQUIRED
            'botId' => 'MAUGGVV7EW', // REQUIRED
            'localeId' => 'en_US', // REQUIRED
            'sessionId' => $sessionid, // REQUIRED
            'sessionState' => [ // REQUIRED
                array (
                    'dialogAction' => 
                    array (
                      'type' => 'Close',
                    ),
                    'intent' => 
                    array (
                      'name' => 'FallbackIntent',
                      'slots' => 
                      array (
                      ),
                      'state' => 'ReadyForFulfillment',
                      'confirmationState' => 'None',
                    ),
                    'originatingRequestId' => 'bb9a370e-9408-46d2-8e52-1e117f540368',
                  ),
                  'interpretations' => array(),
            ],
        ]);


        return to_route('chat');
        // $client = new LexModelsV2Client([
        //     'region' => env('AWS_DEFAULT_REGION'),
        //     'version' => 'latest'
        // ]);

        // $result = $client->deleteBot([
        //     'botId' => 'HKSZL6KGEF', // REQUIRED
        //     'skipResourceInUseCheck' => true,
        // ]);

        // echo "<pre>";
        // print_r($result);
        // echo "</pre>";

        //return view('chat');

    }


    public function chat()
    {
        
        $sessionid = Session::get('sessionid');
        $getdata = User::where('sessionid', $sessionid)->get();
        if(!$getdata){
            return to_route('/');
        }

        $data = array(
          'conversation' => Conversations::where('sessionid', $sessionid)->get(),
          'sessionid' => $sessionid
        );
        return view('chat', $data);
    }

    public function store(Request $request)
    {

        $sessionid = $request->sessionid;
        Conversations::create([
          'sessionid' => $request->sessionid,
          'from' => $sessionid,
          'message' => $request->chat,
        ]);

        $client = new LexRuntimeV2Client([
            'profile' => 'default',
            'version' => 'latest',
            'region'  => 'us-west-2'
        ]);

        $result = $client->recognizeText([
            'botAliasId' => 'TSTALIASID', // REQUIRED
            'botId' => 'MAUGGVV7EW', // REQUIRED
            'localeId' => 'en_US', // REQUIRED
            'sessionId' => $sessionid, // REQUIRED
            'sessionState' => [ // REQUIRED
                array (
                    'dialogAction' => 
                    array (
                      'type' => 'Close',
                    ),
                    'intent' => 
                    array (
                      'name' => 'FallbackIntent',
                      'slots' => 
                      array (
                      ),
                      'state' => 'ReadyForFulfillment',
                      'confirmationState' => 'None',
                    ),
                    'originatingRequestId' => 'bb9a370e-9408-46d2-8e52-1e117f540368',
                  ),
                  'interpretations' => array (),
            ],
            'text' => $request->chat, // REQUIRED
        ]);  

        //print_r($result);

        
        if($result){
          foreach($result['messages'] as $m){
            $resultbot = $m['content'];
          }
        }

       
        Conversations::create([
          'sessionid' => $request->sessionid,
          'from' => 'bot',
          'message' => $resultbot,
        ]);

        $array = array();
        $array['status'] = 'ok';
        $array['data'] = Conversations::all();

        if($result['sessionState']['dialogAction']['type'] == "Close"){
          $result = $client->deleteSession([
              'botAliasId' => 'TSTALIASID', // REQUIRED
              'botId' => 'MAUGGVV7EW', // REQUIRED
              'localeId' => 'en_US', // REQUIRED
              'sessionId' => $sessionid, // REQUIRED
          ]);
         }
      
      return response()->json($array);



    }



    public function getchat(Request $request)
    {

        $sessionid = $request->id;
        $array = array();
        $array['status'] = 'ok';
        $array['data'] = Conversations::where('sessionid', $sessionid)->get();

        return response()->json($array);

    }

    
}
