<?php

use App\Events\MessageCreated;
use App\Models\Conversations;
use App\Http\Controllers\ChatController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [ChatController::class, 'index']);
Route::get('chat', [ChatController::class, 'chat'])->name('chat');
Route::post('store', [ChatController::class, 'store'])->name('store');
Route::post('getchat', [ChatController::class, 'getchat'])->name('getchat');