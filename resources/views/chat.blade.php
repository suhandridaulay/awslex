<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Chat Bot</title>
    <style type="text/css">
		#wrapper
		{
			display: flex;
		  	flex-flow: column;
		  	height: 100%;
		}
		#remaining
		{
			flex-grow : 1;
		}
		#messages {
			height: 200px;
			background: whitesmoke;
			overflow: auto;
		}
		#chat-room-frm {
			margin-top: 10px;
		}
		#user_list
		{
			height:450px;
			overflow-y: auto;
		}

		#messages_area
		{
			height: 500px;
			overflow-y: auto;
			background-color:#e6e6e6;
		}

	</style>
  </head>
  <body>
    <div class="container">
        <div class="row justify-content-center mt-lg-5">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header"><h3>Chat Bot - Order Flower</h3></div>
                    <div class="card-body" id="messages_area"></div>
                    </div>
		
                    <input type="hidden" name="sessionid" value="{{ $sessionid }}">
                    <div class="input-group mb-3">
                        <input class="form-control" id="chat_message" autocomplete="off" name="chat_message" placeholder="Type a message">
                        <button type="button" class="btn btn-primary btn-submit">
                            Send
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script type="text/javascript">
        var sessionid = $("input[name=sessionid]").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        function getchat(id){

            $.ajax({
               type:'POST',
               url:"{{ route('getchat') }}",
               data:{id:id},
               success:function(data){
                    $('.btn-submit').prop('disabled', false);
                    $('#messages_area').empty();

                    if(data.data.length > 0){
                        for (var i = 0; i < data.data.length; i++) {
                            let type= "";
                            let chartbg = "";
                            if(data.data[i].from == sessionid){
                                type= "justify-content-end";
                                chartbg = "alert-light";
                            }else{
                                type= "justify-content-start";
                                chartbg = "alert-success";
                            }

                            $('#messages_area').append(
                                `
                                <div class="row ${type}">
                                    <div class="col-sm-10">
                                        <div class="shadow-sm alert text-dark ${chartbg}">${data.data[i].message}</div>
                                    </div>
                                </div>
                                
                                `
                            );
                        }


                    }

               }
            });

        }

        getchat(sessionid);
   
        
       
        $(".btn-submit").click(function(e){
      
            e.preventDefault();
            $('.btn-submit').prop('disabled', true);
       
            var chat = $("input[name=chat_message]").val();
           
            $('#chat_message').val('');
       
            $.ajax({
               type:'POST',
               url:"{{ route('store') }}",
               data:{chat:chat, sessionid:sessionid},
               beforeSend: function(){
                $('#messages_area').append(`
                    <div class="row justify-content-end">
                        <div class="col-sm-10">
                            <div class="shadow-sm alert text-dark alert-light">${chat}</div>
                        </div>
                    </div>
                `);
               },
               success:function(data){
                    $('.btn-submit').prop('disabled', false);
                    $('#messages_area').empty();
                    getchat(sessionid);
               }
            });
      
        });

        

    </script>
</body>
</html>