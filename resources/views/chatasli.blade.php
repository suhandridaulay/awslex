<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Chat Bot</title>
    <style type="text/css">
		#wrapper
		{
			display: flex;
		  	flex-flow: column;
		  	height: 100%;
		}
		#remaining
		{
			flex-grow : 1;
		}
		#messages {
			height: 200px;
			background: whitesmoke;
			overflow: auto;
		}
		#chat-room-frm {
			margin-top: 10px;
		}
		#user_list
		{
			height:450px;
			overflow-y: auto;
		}

		#messages_area
		{
			height: 650px;
			overflow-y: auto;
			background-color:#e6e6e6;
		}

	</style>
  </head>
  <body>
    <div class="container">
        <div class="row justify-content-center mt-lg-5">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header"><h3>Chat Bot</h3></div>
                    <div class="card-body" id="messages_area">

						@foreach($conversation as $data)
							<div class="row justify-content-end">
								<div class="col-sm-10">
									<div class="shadow-sm alert text-dark alert-light">
									{{ $data->memessage }}
									</div>
								</div>
							</div>

							<div class="row justify-content-start">
								<div class="col-sm-10">
									<div class="shadow-sm alert text-dark alert-success">
										{{ $data->botmessage }}
									</div>
								</div>
							</div>
						@endforeach

                    </div>
                    </div>
					<form action="{{ route('store') }}" method="post" enctype="multipart/form-data">
						@csrf
						<input type="hidden" name="sessionid" value="{{ $sessionid }}">
						<div class="input-group mb-3">
							<textarea class="form-control" id="chat_message" name="chat_message" placeholder="Type a message" data-parsley-maxlength="1000" data-parsley-pattern="/^[a-zA-Z0-9\s]+$/" required></textarea>
							<button type="submit" class="btn btn-primary" >
								Send
							</button>
						</div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>
</html>